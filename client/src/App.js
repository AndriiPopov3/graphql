import React from 'react';
import {Route, Switch } from 'react-router-dom';
import MessageList from './components/Message/MessageList';
import MessageForm from './components/Message/MessageForm';
import './App.css';

function App() {
  return (
    <div className="App">
      <Switch>
        <Route exact path="/" component={MessageList} />
        <Route exact path="/add-message" component={MessageForm} />
      </Switch>
    </div>
  );
}

export default App;
