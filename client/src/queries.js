import gql from 'graphql-tag';

export const MESSAGE_QUERY = gql`
    query messageQuery($orderBy: MessageOrder) {
        messages(orderBy: $orderBy) {
            count
            messageList {
                id
                text
                likeCount
                dislikeCount
                replies {
                    id
                    text
                }
            }
        }
    }
`;

export const FILTER_MESSAGE_QUERY = gql`
    query messageQuery($filter: String, $orderBy: MessageOrder) {
        messages(filter: $filter, orderBy: $orderBy) {
            count
            messageList {
                id
                text
                likeCount
                dislikeCount
                replies {
                    id
                    text
                }
            }
        }
    }
`;

export const POST_MESSAGE_MUTATION = gql`
    mutation PostMutation($text: String!) {
        postMessage(text: $text) {
            id
            text
            likeCount
            dislikeCount
            replies {
                id
                text
            }
        }
    }
`;

export const POST_REPLY_MUTATION = gql`
    mutation PostMutation($messageId: ID!, $text: String!) {
        postReply(messageId: $messageId, text: $text) {
            id
            text
        }
    }
`;

export const NEW_MESSAGES_SUBSCRIPTION = gql`
    subscription {
        newMessage {
            id
            text
            likeCount
            dislikeCount
            replies {
                id
                text
            }
        }
    }
`;

export const NEW_REPLIES_SUBSCRIPTION = gql`
    subscription {
        newReply {
            id
            text 
            message {
                id
            }
        }
    }
`;

export const LIKE_MESSAGE = gql`
    mutation ($messageId: ID!) {
    likeMessage(messageId: $messageId) {
        id
        text
        likeCount
        dislikeCount
        replies {
            id
            text
        }
    }
}`;

export const DISLIKE_MESSAGE = gql`
    mutation ($messageId: ID!) {
    dislikeMessage(messageId: $messageId) {
        id
        text
        likeCount
        dislikeCount
        replies {
            id
            text
        }
    }
}`;