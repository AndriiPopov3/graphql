import React from 'react';
import { useState } from 'react';
import './Header.css';

const Header = ({ filterFunction, filterTextFunction, sortingFunction, sortingFunctionLikes, sortingFunctionDislikes }) => {
    const [checkboxState, setCheckboxState] = useState(false);
    const [checkboxStateLikes, setCheckboxStateLikes] = useState(false);
    const [checkboxStateDislikes, setCheckboxStateDislikes] = useState(false);
    const [searchText, setSearchText] = useState("");
    return (
        <div className="header-container">
            <div className="header-title">Chat</div>
            <div className="header-sort">
                <input type="checkbox" onClick={() => {
                    setCheckboxState(!checkboxState);
                    const orderBy = checkboxState ? false : true;
                    sortingFunction(orderBy);
                }}></input>Sort by date
            </div>
            <div className="header-sort">
                <input type="checkbox" onClick={() => {
                    setCheckboxStateLikes(!checkboxStateLikes);
                    const orderBy = checkboxStateLikes ? false : true;
                    sortingFunctionLikes(orderBy);
                }}></input>Sort by likes ASC
            </div>
            <div className="header-sort">
                <input type="checkbox" onClick={() => {
                    setCheckboxStateDislikes(!checkboxStateDislikes);
                    const orderBy = checkboxStateDislikes ? false : true;
                    sortingFunctionDislikes(orderBy);
                }}></input>Sort by dislikes ASC
            </div>
            <div>
                <input className="search-input" onChange={(event) => {
                    filterTextFunction(event.target.value);
                    setSearchText(event.target.value);
                }}></input>
                <button className="find-messages" onClick={() => {
                    if(searchText !== ""){
                        filterFunction(true)
                    } else {
                        filterFunction(false);
                    }
                }}>Find</button> 
            </div>
        </div>
    );
};

export default Header;