import React from 'react';
import './ReplyItem.css';

const ReplyItem = props => {
    const { text, id } = props;
    return (
        <>
        <div className="reply-item-container">
            <div className="reply-text">{text}</div>
            <div>{"#" + id.slice(id.length - 4, id.length)}</div>
        </div>
        </>
    );
};

export default ReplyItem;