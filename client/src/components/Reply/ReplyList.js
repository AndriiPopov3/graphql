import React, {useState} from 'react';
import ReplyItem from './ReplyItem';
import ReplyForm from './ReplyForm';
import './ReplyList.css';

const ReplyList = ({ messageId, replies }) => {
    const [showReplyForm, toggleForm] = useState(false);
    
    return (
        <div className="reply-list">
            <span className="reply-list-title">Replies</span>
            <button className="reply-button" onClick={() => toggleForm(!showReplyForm)}>
                {showReplyForm ? "Close reply" : "Add reply"}
            </button>
            {replies.map(item => {
                return <ReplyItem key={item.id} {...item} />
            })}
            {showReplyForm && <ReplyForm 
            messageId={messageId}
            toggleForm={toggleForm}
            />}
        </div>
    )
}

export default ReplyList;