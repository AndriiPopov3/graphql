import React, {useState} from 'react';
import { Mutation } from 'react-apollo';
import { POST_MESSAGE_MUTATION, MESSAGE_QUERY } from '../../queries';
import './MessageForm.css';

const MessageForm = (props) => {
    const [text, setText] = useState("");

    const _updateStoreAfterAddingMessage = (store, newMessage) => {
        const orderBy = 'createdAt_ASC';
        const data = store.readQuery({
            query: MESSAGE_QUERY,
            variables: {
                orderBy
            }
        });
        data.messages.messageList.push(newMessage);
        store.writeQuery({
            query: MESSAGE_QUERY,
            data,
        });
    };

    return (
        <div className="form-container">
            <div className="form-block">
                <div className="input-wrapper">
                    <input className="form-input" type="text" placeholder="Text" value={text} onChange={e => setText(e.target.value)} />
                </div>

                <Mutation 
                    mutation={POST_MESSAGE_MUTATION}
                    variables={{text}}
                    update={(store, {data: { postMessage } }) => {
                        _updateStoreAfterAddingMessage(store, postMessage);
                    }}  
                    onCompleted={() => props.history.push('/')}
                >
                    {postMutation => 
                        <button className="form-send-button" onClick={postMutation}>Send</button>}
                </Mutation>
            </div>
        </div>
    );
};

export default MessageForm;