import React from 'react';
import { Query } from 'react-apollo';
import { Link } from 'react-router-dom';
import { useState } from 'react';
// import { useLazyQuery } from '@apollo/react-hooks';
import MessageItem from './MessageItem';
import Header from '../Header/Header';
import { MESSAGE_QUERY, FILTER_MESSAGE_QUERY, NEW_MESSAGES_SUBSCRIPTION, NEW_REPLIES_SUBSCRIPTION } from '../../queries';
import './MessageList.css';

const MessageList = () => {
    // const [getFilteredMessages, { loading, data }] = useLazyQuery(FILTER_MESSAGE_QUERY);
    const [descSort, setDescSort] = useState(false);
    const [likeSort, setLikeSort] = useState(false);
    const [dislikeSort, setDislikeSort] = useState(false);
    const [filterFlag, setFilterFlag] = useState(false);
    const [filterText, setFilterText] = useState("");

    const _subscribeToNewMessage = subscribeToMore => {
        subscribeToMore({
            document: NEW_MESSAGES_SUBSCRIPTION,
            updateQuery: (prev, { subscriptionData }) => {
                if (!subscriptionData.data) return prev;
                const { newMessage } = subscriptionData.data;
                const exists = prev.messages.messageList.find(({ id }) => id === newMessage.id);
                if (exists) return prev;

                return {...prev, messages: {
                        messageList: [...prev.messages.messageList, newMessage],
                        count: prev.messages.messageList.length + 1,
                        __typename: prev.messages.__typename
                }};
            }
        });
    };

    const _subscribeToNewReply = subscribeToMore => {
        subscribeToMore({
            document: NEW_REPLIES_SUBSCRIPTION,
            updateQuery: (prev, { subscriptionData }) => {
                if (!subscriptionData.data) return prev;
                const { newReply } = subscriptionData.data;
                let exists = false;
                for (let i = 0; i < prev.messages.messageList.length; i++) {
                    prev.messages.messageList[i].replies.map(reply => reply.id === newReply.id ? exists = true : null)
                    if (exists) break;
                }
                if (exists) return prev;
                console.log(newReply, prev);
                const newMessages = prev.messages.messageList.map(message => {
                    if (message.id === newReply.message.id) {
                        message.replies.push(newReply)
                        console.log("push");
                    }
                    return message;
                });
                return {...prev, messages: {
                        messageList: newMessages,
                        count: prev.messages.messageList.length,
                        __typename: prev.messages.__typename
                }};
            }
        });
    };
    let orderBy = likeSort ? "likeCount_DESC" : null;
    if (!orderBy) {
        orderBy = dislikeSort ? "dislikeCount_DESC" : null;
    }
    if (!orderBy) {
        orderBy = descSort ? "createdAt_DESC" : "createdAt_ASC";
    }
    return filterFlag ? 
    (
        <Query query={FILTER_MESSAGE_QUERY} variables={{ filter: filterText }}>
            {({loading, error, data, subscribeToMore }) => {
                if (loading) return <div>Loading...</div>;
                if (error) return <div>Fetch error</div>;
                _subscribeToNewMessage(subscribeToMore);
                _subscribeToNewReply(subscribeToMore);
                const { messages: { messageList } } = data;
                
                return (
                    <>
                    <Header
                        filterFunction={setFilterFlag}
                        filterTextFunction={setFilterText}
                        sortingFunction={setDescSort}
                        sortingFunctionLikes={setLikeSort} 
                        sortingFunctionDislikes={setDislikeSort}    
                    />
                    <div className="message-list">
                        {messageList.map(item => {
                            return <MessageItem key={item.id} {...item} />
                        })}
                    </div>
                    
                    <div className="button-container">
                        <Link to="/"><button className="button-to-chat">Chat</button></Link>
                        <Link to="/add-message"><button className="button-to-send">Send message</button></Link>
                    </div>
                    </>
                )
            }}
        </Query>
    )
    :
    (
        <Query query={MESSAGE_QUERY} variables={{ orderBy: orderBy }}>
            {({loading, error, data, subscribeToMore }) => {
                if (loading) return <div>Loading...</div>;
                if (error) return <div>Fetch error</div>;
                _subscribeToNewMessage(subscribeToMore);
                _subscribeToNewReply(subscribeToMore);
                const { messages: { messageList } } = data;
                
                return (
                    <>
                    <Header
                        filterFunction={setFilterFlag}
                        filterTextFunction={setFilterText}
                        sortingFunction={setDescSort}
                        sortingFunctionLikes={setLikeSort}
                        sortingFunctionDislikes={setDislikeSort}   
                    />
                    <div className="message-list">
                        {messageList.map(item => {
                            return <MessageItem key={item.id} {...item} />
                        })}
                    </div>
                    
                    <div className="button-container">
                        <Link to="/"><button className="button-to-chat">Chat</button></Link>
                        <Link to="/add-message"><button className="button-to-send">Send message</button></Link>
                    </div>
                    </>
                )
            }}
        </Query>
    )
}

export default MessageList;