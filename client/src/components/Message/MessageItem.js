import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faThumbsUp, faThumbsDown } from '@fortawesome/free-solid-svg-icons'
import ReplyList from '../Reply/ReplyList';
import { useMutation } from '@apollo/react-hooks';
import { LIKE_MESSAGE, DISLIKE_MESSAGE } from '../../queries';
import './MessageItem.css';

const MessageItem = ({ id, text, likeCount, dislikeCount, replies }) => {
    const [likeMessage, { loading, data }] = useMutation(LIKE_MESSAGE);
    const [dislikeMessage, { loading2, data2 }] = useMutation(DISLIKE_MESSAGE);
    return (
        <div className="message-item">
            <div className="message-text-container">
                {text}
                <ReplyList messageId={id} replies={replies} />
            </div>
            <div className="message-reactions-container">
                <div className="message-id">
                    {"#" + id.slice(id.length - 4, id.length)}
                </div>
                <div className="message-react-container">  
                    <FontAwesomeIcon
                        icon={faThumbsUp}
                        className="message-like-icon"
                        onClick={() => likeMessage({variables: { messageId: id } })}
                    />
                    <div>{likeCount}</div>
                </div>
                <div className="message-react-container">
                    <FontAwesomeIcon
                        icon={faThumbsDown}
                        className="message-dislike-icon"
                        onClick={() => dislikeMessage({variables: { messageId: id } })}
                    />
                    <div>{dislikeCount}</div>
                </div>
            </div>
            
        </div>
    )
}

export default MessageItem;