const { GraphQLServer } = require('graphql-yoga');
const { prisma } = require("./generated/prisma-client");
const Query = require('./resolvers/Query');
const Mutation = require('./resolvers/Mutation');
const Subscription = require('./resolvers/Subscription');
const Reply = require('./resolvers/Reply');
const Message = require('./resolvers/Message');

const resolvers = {
    Query,
    Mutation,
    Subscription,
    Message,
    Reply
};

const server = new GraphQLServer({
    typeDefs: './schema.graphql',
    resolvers,
    context: { prisma }
});

server.start(() => console.log("http://localhost:4000"));

// query {
//     messages {
//       messageList {
//         id text likeCount dislikeCount replies {
//           id text
//         } 
//       }
//       count
//     }
//   }