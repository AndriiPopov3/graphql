const { messages } = require("./Query");

function postMessage(parent, args, context, info) {
    return context.prisma.createMessage({
        text: args.text,
        likeCount: 0,
        dislikeCount: 0
    });
}

async function postReply(parent, args, context, info) {
    const messageExists = await context.prisma.$exists.message({
        id: args.messageId
    });

    if (!messageExists) {
        throw new Error(`Message with ID ${args.messageId} does not exist`);
    }

    return context.prisma.createReply({
        text: args.text,
        message: {connect: { id: args.messageId } }
    });
}

async function likeMessage(parent, args, context, info) { 
    const message = await context.prisma.message({
        id: args.messageId
    });
    const currentLikes = message.likeCount;
    const updatedMessage = await context.prisma.updateMessage({
        where: {
          id: args.messageId
        },
        data: {
          likeCount: currentLikes + 1,
        },
      });
      return updatedMessage;
}

async function dislikeMessage(parent, args, context, info) { 
    const message = await context.prisma.message({
        id: args.messageId
    });
    const currentDislikes = message.dislikeCount;
    const updatedMessage = await context.prisma.updateMessage({
        where: {
          id: args.messageId
        },
        data: {
          dislikeCount: currentDislikes + 1,
        },
      });
      return updatedMessage;
}

module.exports = {
    postMessage,
    postReply,
    likeMessage,
    dislikeMessage
}